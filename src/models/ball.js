import CircleCollider from "../collision/circle_collider.js";
import { ColliderEvent } from "../collision/collider.js";
import { GAME_HEIGHT, GAME_TOP_Y, GAME_WIDTH, ITEM_DIRECTION_WORK, ITEM_SHIELD_WORK } from "../constant.js";
import {
    calcAngle,
    calcDistance,
    calcVector,
    getSpriteFromCache,
    rotate,
} from "../utils/utils.js";
import GameObject from "./game_object.js";

export const BallEvent = Object.freeze({
    Stop: "ball:stop",
    Colliding: "ball:colliding"
})

export class Ball extends GameObject {
    constructor(radius, speed = 10) {
        super();
        this.radius = radius;
        this.speed = speed;

        this.sprite = getSpriteFromCache("ball");
        this.scaleRatio = (this.radius * 2) / this.sprite.width;
        this.sprite.scale.set(this.scaleRatio);

        this.addChild(this.sprite);
        this.moving = false;
        this.notMoveCount = 0;
        this.x = 0;
        this.y = 0;

        this.preX = this.x;
        this.preY = this.y;
        this.collider = new CircleCollider(this.x, this.y, this.radius);
        this.collider.on(ColliderEvent.Colliding, this.onCollision, this)

        this.moved = false;
        this.isShielding = false;
        this.listItemActivated = [];

        this.needToCheckCollision = true;
    }

    update(delta) {
        super.update(delta);
        
        this.collider.x = this.x;
        this.collider.y = this.y;

        //handle collision
        let edgeCollision = this.collider.wallCollision();
        if (edgeCollision) this.onCollision(edgeCollision, true);

        //define ball moved or not
        let distance = calcDistance(this.x, this.y, this.preX, this.preY);
        if(distance > 2*this.radius){
            this.moved = true
            if(!this.visible)
                this.visible = true;
        }

        //ball stop
        if(this.moving && this.moved && this.vx === 0 && this.vy === 0 && this.visible){
            
            this.emit(BallEvent.Stop, this);
            if(this.checkItemActivated(ITEM_SHIELD_WORK))
                this.removeChild(this.shield)
            this.listItemActivated = [];
        }
    }

    setPosition(x, y){
        super.setPosition(x, y);
        this.preX = this.x;
        this.preY = this.y;
        this.collider.x = this.x;
        this.collider.y = this.y;
    }

    onCollision(value, withWall = false) {
        if (withWall) {
            if (value.indexOf("left") !== -1 && this.vx < 0) {
                this.vx = -this.vx;
            }
            if (value.indexOf("right") !== -1 && this.vx > 0) {
                this.vx = -this.vx;
            }
            if (value.indexOf("top") !== -1 && this.vy < 0) {
                this.vy = -this.vy;
            }
            if (value.indexOf("bottom") !== -1) {
                if (this.listItemActivated.includes(ITEM_SHIELD_WORK) && this.isShielding && this.needToCheckCollision) {
                    this.isShielding = false;
                    this.removeChild(this.shield)
                    this.vy = -this.vy;
                } else if(this.moved){
                    this.vx = 0;
                    this.vy = 0;
                }
            }
        } else if(value && this.needToCheckCollision){
            let rect = value;
            //left
            if (
                this.x < rect.x - rect.width / 2 &&
                this.y > rect.y - rect.height / 2 &&
                this.y < rect.y + rect.height / 2 &&
                this.vx > 0
            ) {
                this.vx = -this.vx;
            }

            //right
            else if (
                this.x > rect.x + rect.width / 2 &&
                this.y > rect.y - rect.height / 2 &&
                this.y < rect.y + rect.height / 2 &&
                this.vx < 0
            )
                this.vx = -this.vx;

            //top
            else if (
                this.x > rect.x - rect.width / 2 &&
                this.x < rect.x + rect.width / 2 &&
                this.y + this.radius > rect.y - rect.height / 2 &&
                this.vy > 0
            ){
                this.vy = -this.vy;
            }
                

            //bottom
            else if (
                this.x > rect.x - rect.width / 2 &&
                this.x < rect.x + rect.width / 2 &&
                this.y - this.radius < rect.y + rect.height / 2 &&
                this.vy < 0
            ) {
                this.vy = -this.vy;
            }

            // top left
            else if (
                this.x < rect.x - rect.width / 2 &&
                this.y < rect.y - rect.height / 2
            ) {
                if (this.vx > 0) this.vx = -this.vx;
                if (this.vy > 0) this.vy = -this.vy;
            }
            // top right
            else if (
                this.x > rect.x + rect.width / 2 &&
                this.y < rect.y - rect.height / 2
            ) {
                if (this.vx < 0) this.vx = -this.vx;
                if (this.vy > 0) this.vy = -this.vy;
            }

            // bottom left
            else if (
                this.x < rect.x - rect.width / 2 &&
                this.y > rect.y + rect.height / 2
            ) {
                if (this.vx > 0) this.vx = -this.vx;
                if (this.vy < 0) this.vy = -this.vy;
            }

            // bottom right
            else if (
                this.x > rect.x + rect.width / 2 &&
                this.y > rect.y + rect.height / 2
            ) {
                if (this.vx < 0) this.vx = -this.vx;
                if (this.vy < 0) this.vy = -this.vy;
            }
        }
    }

    calcVelocity(radian) {
        this.preX = this.x;
        this.preY = this.y;
        this.vx = Math.cos(radian) * this.speed;
        this.vy = -Math.sin(radian) * this.speed;
        this.listItemActivated = []
        this.needToCheckCollision = true;
    }

    copy() {
        let ball = new Ball(this.radius, this.speed);
        ball.setPosition(this.x, this.y);
        ball.setVelocity(this.vx, this.vy);
        ball.moving = true;
        ball.visible = true;
        return ball;
    }

    checkItemActivated(itemWork){
        return this.listItemActivated.includes(itemWork)
    }

    setItemActivated(itemWork){
        if(this.listItemActivated.includes(itemWork))
            return
        this.listItemActivated.push(itemWork)
        if(itemWork === ITEM_SHIELD_WORK){
            this.shield = getSpriteFromCache('bound_circle_yellow');
            this.isShielding = true;
            this.shield.scale.set(this.scaleRatio)
            this.addChild(this.shield);
        }
    }

    removeItemWork(itemWork){
        let index = this.listItemActivated.indexOf(itemWork);
        if(index !== -1){
            this.listItemActivated.splice(index, 1)
        }
    }

    setStop(x = this.x, y = this.y, force = false) {
        if((this.vx === 0 && this.vy === 0 && this.moved) || force){
            this.x = x;
            this.y = y;
            this.vx = 0;
            this.vy = 0;
            this.visible = false;
            this.moving = false;
            this.preX = this.x;
            this.preY = this.y;
            this.moved = false;
            if(this.shield){
                this.removeChild(this.shield);
            }
        }
    }
}
