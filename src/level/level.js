import { Container, Graphics, Rectangle } from "pixi.js";
import { GameAnchor, GAME_HEIGHT, GAME_TOP_Y, GAME_WIDTH } from "../constant";
import BallManager, { BallManagerEvent } from "../managers/ball_manager";
import BoardManager, { BoardManagerEvent } from "../managers/board_manager";
import PredictLine from "../models/predict_line";
import {
    calcAngle,
    calcVector,
    getSpriteFromCache,
    intersect,
    rotate,
} from "../utils/utils";
import * as Constants from "../constant";
import CollisionManager, {
    ColliderType,
    CollisionManagerEvent,
} from "../collision/collision_manager";
import LineManager from "../managers/lline_manager";
import EffectManager from "../effect/effect_manager";

export const LevelEvent = Object.freeze({
    Start: "level:start",
    Complete: "level:complete",
    Failure: "level:failure",
    Scored: "level:scored"
});

export default class Level extends Container {
    constructor(data) {
        super();
        this.numOfBall = data.num_of_ball;
        this.items = data.items;

        this._initBallManager();
        this._initPredictLine();
        this._initBoardManagers();
        this._initCollisionManager();
        this._initButtonEndTurn();
        this._initLineManager();
        this._initEvent();
        this.shootTurn = true;
        this.numBrickBreakInTurn = 0;
        this.needUpdate = true;
        this.effectManager = new EffectManager();
        this.addChild(this.effectManager)
    }

    _initBoardManagers() {
        this.boardManager = new BoardManager(this.items);
        this.addChild(this.boardManager);
        this.boardManager.once(
            BoardManagerEvent.Clear,
            this._levelComplete,
            this
        );
    }

    _initBallManager() {
        this.ballManager = new BallManager(this.numOfBall);
        this.addChild(this.ballManager);
    }

    _initPredictLine() {
        this.predictLine = new PredictLine();
        this.addChild(this.predictLine);
        this.predictLine.setPosition(
            this.ballManager.rootBall.x,
            this.ballManager.rootBall.y
        );

        this.interactive = true;
        this.hitArea = new Rectangle(0, GAME_TOP_Y, GAME_WIDTH, GAME_HEIGHT);
    }

    _initCollisionManager() {
        this.collisionManager = new CollisionManager();
        this.boardManager.listObjects.forEach((object) => {
            this.collisionManager.add(object.collider);
        });

        this.ballManager.listBalls.forEach((ball) => {
            this.collisionManager.add(ball.collider, ColliderType.Dynamic);
        });
    }
    
    _initButtonEndTurn(){
        this.btnEndTurn = getSpriteFromCache('btn_cancel_move')
        this.btnEndTurn.position.set(GAME_WIDTH/2, GAME_HEIGHT + GAME_TOP_Y/2)
        this.btnEndTurn.interactive = true;
        this.btnEndTurn.buttonMode = true;
        this.btnEndTurn.visible = false;
        this.addChild(this.btnEndTurn)
    }

    _initEvent() {
        this.boardManager.once(BoardManagerEvent.End, this._levelFail, this);
        this.ballManager.on(BallManagerEvent.Stop, this.endTurn, this);

        this.interactive = true;
        this.on("pointerdown", this.dragStart, this);
        this.on("pointerup", this.dragEnd, this);

        this.boardManager.on(
            BoardManagerEvent.RemoveChild,
            this.collisionManager.remove,
            this.collisionManager
        );

        this.boardManager.on(
            BoardManagerEvent.BrickBreak,
            this.onBrickBreak,
            this
        )
        this.boardManager.on(
            BoardManagerEvent.ItemActivated,
            this.onItemActivated,
            this
        );

        this.btnEndTurn.on("pointerdown", this.stopTurn, this)
    }

    _initLineManager() {
        this.lineManager = new LineManager();
        this.addChild(this.lineManager);
    }

    stopTurn(){
        this.needUpdate = false
        this.ballManager.stopMove()
        
    }

    onBrickBreak(brick){
        this.numBrickBreakInTurn++;
        let x = brick.x;
        let y = brick.y;
        this.effectManager.brickBreakEffect(x, y);
        this.emit(LevelEvent.Scored, this.numBrickBreakInTurn*10)
    }

    onItemActivated(item, dynamicCollider) {
        switch (item.work) {
            case Constants.ITEM_BALL_WORK:
                let ball = this.ballManager.addBall();
                this.collisionManager.add(ball.collider, ColliderType.Dynamic);
                break;
            case Constants.ITEM_SHIELD_WORK:
                this.ballManager.setItemActivated(item.work, dynamicCollider)
                break;
            case Constants.ITEM_ROW_WORK:
                this.lineManager.horizontalAt(item.y);
                this.boardManager.activeRow(item.row);
                break;
            case Constants.ITEM_COL_WORK:
                this.lineManager.verticalAt(item.x);
                this.boardManager.activeCol(item.col)
                break;
        }
    }

    endTurn() {
        this.predictLine.setPosition(
            this.ballManager.rootBall.x,
            this.ballManager.rootBall.y
        );
        this.shootTurn = true;
        this.needUpdate = true;
        this.boardManager.nextMove();
        this.btnEndTurn.visible = false;
    }

    dragStart(e) {
        e = e.data.global;
        if(e.y > Constants.APP_HEIGHT - GAME_TOP_Y )
            return
        if (this.shootTurn) {
            this.numBrickBreakInTurn = 0;
            this.predictLine.draw(e);
            this.on("pointermove", this.dragMove, this);
        }
    }

    dragMove(e) {
        e = e.data.global;
        if (this.shootTurn) {
            this.predictLine.draw(e);
        }
    }

    dragEnd() {
        if (this.shootTurn && this.predictLine.angleShoot) {
            this.ballManager.shoot(this.predictLine.angleShoot);
            this.predictLine.angleShoot = false;
            this.predictLine.clear();
            this.removeListener("pointermove");
            this.shootTurn = false;
            this.btnEndTurn.visible = true;
        }
    }

    update(delta) {
        if(this.needUpdate){
            this.collisionManager.update();
            this.ballManager.update(delta);
            this.effectManager.update(delta);
        }
    }

    _levelComplete() {
        this.emit(LevelEvent.Complete, this);
    }

    _levelFail() {
        this.emit(LevelEvent.Failure, this);
    }
}
