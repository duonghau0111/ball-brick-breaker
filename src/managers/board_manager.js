import { Container } from "pixi.js";
import { BLOCK_SIZE, GAME_HEIGHT, GAME_WIDTH, ITEM_BALL_WORK, NUM_ROW } from "../constant";
import Brick from "../models/brick";
import { GameObjectEvent } from "../models/game_object";
import Item, { ItemEvent } from "../models/item";

export const BoardManagerEvent = Object.freeze({
    Clear: "boardmanager:clear",
    End: "boardmanager:end",
    RemoveChild: "boardmanager:removechild",
    ItemActivated: 'boardmanager:itemavtivated',
    BrickBreak: "boardmanager:brickbreak"
});

export default class BoardManager extends Container {
    constructor(items) {
        super();
        this.items = items;
        this._init();
    }

    _init() {
        this.numCol = parseInt(GAME_WIDTH / BLOCK_SIZE) - 1;
        this.numRow = parseInt(GAME_HEIGHT / BLOCK_SIZE) - 3;

        this.listObjects = [];
        this.items.forEach((e) => {
            let object;
            let data = e.data;
            if (e.type == "brick") {
                object = new Brick(data.weight);
                object.setPositionOnGrid(data.row, data.col);
            } else if (e.type == "item") {
                object = new Item(
                    data.work,
                    data.size,
                    data.child,
                    data.hasBoundingCircle
                );
                object.setPositionOnGrid(data.row, data.col);
                object.on(ItemEvent.Active, this.onItemActivated, this)
            }

            object.on(GameObjectEvent.NeedRemove, this.remove, this)
            this.addChild(object);
            this.listObjects.push(object);
        });
    }

    onItemActivated(item, dynamicCollider){
        if(item.work === ITEM_BALL_WORK){
            this.remove(item)
        }
        this.emit(BoardManagerEvent.ItemActivated, item, dynamicCollider)
    }

    remove(object) {
        let index = this.listObjects.indexOf(object);
        this.listObjects.splice(index, 1);
        this.removeChild(object);
        if(object.type === 'brick'){
            this.emit(BoardManagerEvent.BrickBreak, object)
        }
        this.emit(BoardManagerEvent.RemoveChild, object)
    }

    getObjectByCollider(collider){
        
        for (let i = 0; i < this.listObjects.length; i++) {
            let object = this.listObjects[i]
            if(object.collider === collider){
                return object;
            }
        }
    }

    nextMove() {
        this.listObjects.forEach((object) => {
            object.moveBottom();
        });
        this._removeActivatedItem()
        this._checkDoneBoard();
    }

    _removeActivatedItem(){
        this.listObjects.forEach(object => {
            if(object.type === "item"){
                if(object.activated){
                    this.remove(object)
                }
            }
        })
    }

    _checkDoneBoard() {
        let countBrick = 0
        this.listObjects.forEach((object) => {
            if (object.type === "brick") {
                countBrick++;
                if (object.row === NUM_ROW) this.emit(BoardManagerEvent.End);
            }
        });
        if(countBrick === 0){
            this.emit(BoardManagerEvent.Clear)
        }
    }

    activeCol(col){
        this.listObjects.forEach(object =>{
            if(object.type === "brick")
                if(object.col === col){
                    object.updateWeight(object.weight-1);
                }
        })
    }

    activeRow(row){
        this.listObjects.forEach(object =>{
            if(object.type === "brick")
                if(object.row === row){
                    object.updateWeight(object.weight-1);
                }
        })
    }
}
