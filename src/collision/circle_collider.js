import {
    CIRCLE_COLLIDER,
    GAME_HEIGHT,
    GAME_TOP_Y,
    GAME_WIDTH,
    RECT_COLLIDER,
} from "../constant";
import { calcDistance, calcVector } from "../utils/utils";
import Collider, { ColliderEvent } from "./collider";

export default class CircleCollider extends Collider {
    constructor(x, y, radius) {
        super(CIRCLE_COLLIDER, x, y);
        this.radius = radius;
        this.listColliders = [];
    }

    checkCollision(objectToCheck) {
        let isColliding;
        if (objectToCheck.type === CIRCLE_COLLIDER) {
            isColliding = this.circleCollision(objectToCheck);
        } else if (objectToCheck.type === RECT_COLLIDER) {
            isColliding = this.rectCollision(objectToCheck);
        }
        this.preventMultipleCollision(objectToCheck, isColliding);
        return isColliding;
    }

    //prevent collision between two objects in same time
    preventMultipleCollision(collider, isColliding){
        if(isColliding){
            if(!this.listColliders.includes(collider)){
                this.listColliders.push(collider);
                this.emit(ColliderEvent.Colliding, collider);
            }
        }
        else{
            if(this.listColliders.includes(collider)){
                let index = this.listColliders.indexOf(collider);
                this.listColliders.splice(index, 1);
            }
        }
        
    }

    circleCollision(object) {
        let distance = calcDistance(this.x, this.y, object.x, object.y);
        let isColliding = distance <= this.radius + object.radius;
        return isColliding;
    }

    rectCollision(rect) {
        let distX = Math.abs(this.x - rect.x);
        let distY = Math.abs(this.y - rect.y);

        if (distX > rect.width / 2 + this.radius) {
            return false;
        }
        if (distY > rect.height / 2 + this.radius) {
            return false;
        }

        if (distX <= rect.width / 2) {
            return rect;
        }
        if (distY <= rect.height / 2) {
            return rect;
        }

        let dx = distX - rect.width / 2;
        let dy = distY - rect.height / 2;

        if (dx * dx + dy * dy <= this.radius * this.radius) return rect;
        return false;
    }

    wallCollision() {
        let edgeCollision = [];
        // check collision with left board
        if (this.x - this.radius - 4 <= 0) {
            edgeCollision.push("left");
        }
        // check collision with right board
        else if (this.x + this.radius + 4 >= GAME_WIDTH) {
            edgeCollision.push("right");
        }

        // check collision with top board
        if (this.y - this.radius - 4 <= GAME_TOP_Y) {
            edgeCollision.push("top");
        }

        // check collision with bottom board
        else if (this.y + this.radius + 4 > GAME_HEIGHT) {
            edgeCollision.push("bottom");
        }

        if (edgeCollision.length === 0) return false;
        else return edgeCollision;
    }
}