export const BLOCK_SIZE = 38;
export const BALL_RADIUS = 7;
export const GAME_WIDTH = 360;
export const GAME_HEIGHT = 520;
export const GAME_TOP_Y = 80;
export const APP_HEIGHT = 600;
export const DEFAULT_PADDING = 2;
export const NUM_ROW = 11;
export const NUM_COL = 9;

export const ITEM_BALL_WORK = 'ball';
export const ITEM_ROW_WORK = 'row';
export const ITEM_COL_WORK = 'col';
export const ITEM_DIRECTION_WORK = 'direction';
export const ITEM_SHIELD_WORK = 'shield';
export const ITEM_PLUS_WORK = 'plus';

export const CIRCLE_COLLIDER = 'circle';
export const RECT_COLLIDER = 'rect';

export const GameAnchor =  Object.freeze({
    topLeft: {
        x: 0,
        y: GAME_TOP_Y,
    },
    topRight: {
        x: GAME_WIDTH,
        y: GAME_TOP_Y,
    },
    bottomLeft: {
        x: 0,
        y: GAME_HEIGHT,
    },
    bottomRight: {
        x: GAME_WIDTH,
        y: GAME_HEIGHT,
    }
})


